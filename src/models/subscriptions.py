"""Subscription related models and database functionality"""
from enum import Enum
from typing import Dict, List, Optional, Tuple
from datetime import datetime
from flask.globals import current_app, session

from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy import event
from sqlalchemy.orm.query import Query
from flask_sqlalchemy import models_committed, before_models_committed
from sqlalchemy.orm.attributes import get_history

from src.models.base import db
from src.models.utils import get_or_not_found
from src.models.service_codes import subscriptions_service_codes
from src.models.usages import DataUsage
from src.models.cycles import BillingCycle


class SubscriptionStatus(Enum):
    """Enum representing possible subscription statuses"""
    new = "new"
    active = "active"
    suspended = "suspended"
    expired = "expired"


class Subscription(db.Model):
    """Model class to represent ATT subscriptions"""

    __tablename__ = "subscriptions"

    id = db.Column(db.Integer, primary_key=True)
    phone_number = db.Column(db.String(10))
    status = db.Column(ENUM(SubscriptionStatus), default=SubscriptionStatus.new)
    activation_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)
    expiry_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)

    plan_id = db.Column(db.String(30), db.ForeignKey("plans.id"), nullable=False)
    plan = db.relationship("Plan", foreign_keys=[plan_id], lazy="select")
    service_codes = db.relationship(
        "ServiceCode", secondary=subscriptions_service_codes,
        primaryjoin="Subscription.id==subscriptions_service_codes.c.subscription_id",
        secondaryjoin="ServiceCode.id==subscriptions_service_codes.c.service_code_id",
        back_populates="subscriptions", cascade="all,delete", lazy="subquery"
    )

    data_usages = db.relationship(DataUsage, back_populates="subscription")

    _is_new_version = False # field for checking when plan_id was updated and we need to create new version for subscription

    def __repr__(self):  # pragma: no cover
        return (
            f"<{self.__class__.__name__}: {self.id} ({self.status}), "
            f"phone_number: {self.phone_number or '[no phone number]'}, "
            f"plan: {self.plan_id}>"
        )

    @classmethod
    def get_subscriptions(cls, **kwargs):
        """Gets a list of Subscription objects using given kwargs

        Generates query filters from kwargs param using base class method

        Args:
            kwargs: key value pairs to apply as filters

        Returns:
            list: objects returned from query result

        """
        return cls.query.filter(**kwargs).all()

    @property
    def service_code_names(self):
        """Helper property to return names of active service codes"""
        return [code.name for code in self.service_codes]


class SubscriptionVersion(db.Model):
    """Model class that represent versioning table for subscription and plan"""
    __tablename__ = "subscription_versions"
    id = db.Column(db.Integer, primary_key=True)
    plan_id = db.Column(db.String(30), db.ForeignKey("plans.id"), nullable=False)
    subscription_id = db.Column(db.Integer, db.ForeignKey("subscriptions.id"), nullable=False)
    start_effective_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    end_effective_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)
    created_at = db.Column(db.TIMESTAMP(timezone=True))
    plan = db.relationship("Plan",cascade="all,delete",lazy='joined')

    def __repr__(self):  # pragma: no cover
        return (
            f"<{self.__class__.__name__}: {self.id} | {self.created_at} "
            f"(Sub: {self.subscription_id} Plan: {self.plan_id})>"
        )

    @classmethod
    def _query_by_billing_cycle(cls, billing_cycle: BillingCycle) -> Query:
        query = cls.query.filter(
            cls.start_effective_date <= billing_cycle.end_date,
            cls.end_effective_date >= billing_cycle.start_date,    
        )
        return query

    @classmethod
    def get_subscription_versions(cls, billing_cycle: BillingCycle, subscription : Subscription) -> List[Tuple]:
        """
        Get subscription versions inside billing cycle.

        Args:
            billing_cycle (BillingCycle): billing cycle
            subscription (Subscription): subscription for which get versions
        
        Returns:
            objects (List[Tuple]): returned from query

        """
        query = cls._query_by_billing_cycle(billing_cycle).filter(
            cls.subscription_id == subscription.id,
        )
        result = [
            (i.plan.mb_available, i.start_effective_date, i.end_effective_date)
            for i in query.all()
        ]
        return result

    @classmethod
    def get_plans_versions(cls, billing_cycle: BillingCycle) -> Dict:
        """
        Get all active subscription inside billing cycle versions grouped by plan.mb_available.

        Args:
            billing_cycle (BillingCycle): billing cycle
           
        Returns:
            objects (Dict): returned from dict format with the plan 
                size as the key and a list of the subscription ids as the value

        """
        query = cls._query_by_billing_cycle(billing_cycle).join(
            Subscription
        ).filter(Subscription.status == SubscriptionStatus.active)
        plans = {}
        for i in query.all():
            plans.setdefault(i.plan.mb_available, []).append(i.subscription_id)
        return plans

    @classmethod
    def add_subscription_version(cls, subscription: Subscription) -> None:
        """
        Create a new version for subscription.

            Args:
                subscription_id (int): subscription if for which create a new version
                plan_id (int): plan id of new plan for subscription
        """
        current_time = datetime.utcnow()
        expiration_time = BillingCycle.get_current_cycle(datetime.now()).end_date
        if subscription.expiry_date and subscription.expiry_date < expiration_time:
            expiration_time = subscription.expiry_date
        old_version = db.session.query(cls).filter(
            cls.subscription_id == subscription.id,
        ).order_by(cls.created_at.desc()).first()
        if old_version:
            old_version.end_effective_date = current_time
        new_version = cls(
            plan_id=subscription.plan_id,
            subscription_id=subscription.id,
            start_effective_date=current_time,
            end_effective_date=expiration_time,
            created_at=current_time
        )
        db.session.add(new_version)
        subscription._is_new_version = False


@event.listens_for(Subscription.plan_id, 'set', active_history=True)
def rplan_id_set(target, value, old_value, initiator):
    if old_value != value:
        target._is_new_version = True


def notify_subscribers(app, changes):
    old_session = db.session
    db.session = db.create_scoped_session()
    for instance in (obj for obj, state in changes if isinstance(obj, Subscription) and obj._is_new_version):
        SubscriptionVersion.add_subscription_version(instance)
    db.session.commit()
    db.session.remove()
    db.session = old_session

models_committed.connect(notify_subscribers)
