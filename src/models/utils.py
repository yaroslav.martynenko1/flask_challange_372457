"""Utilities for models to inherit or use"""
from sqlalchemy.exc import SQLAlchemyError
from flask import abort
from http import HTTPStatus


def get_object_or_404(model, mid):
    """Get an object by id or return a 404 not found response

    Args:
        model (object): object's model class
        mid (int): object's id

    Returns:
        object: returned from query

    Raises:
        404: if one object is returned from query

    """
    try:
        return model.query.one(pk=mid)
    except SQLAlchemyError:
        abort(404)


class NotFoundError(Exception):
    def __init__(self, model, id, *args: object) -> None:
        super().__init__(f'No {model} with id:{id}' ,*args)


def get_or_not_found(model, id):
    """
    Get object from db by id

    Args:
        model (object): object's model class
        mid (int): object's id

    Returns:
        object: returned from query

    Raises:
        NotFoundError: if one object is returned from query
    """
    result = model.query.get(id)
    if result is None:
        raise NotFoundError(model, id)
    return result