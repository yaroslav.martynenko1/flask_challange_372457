"""Plan related tasks"""
from typing import Dict, List, Optional, Tuple, Union
from celery.utils.log import get_task_logger
from flask import current_app

from src.celery_app import celery
from src.models.subscriptions import SubscriptionVersion, Subscription
from src.models.cycles import BillingCycle
from src.models.utils import get_or_not_found

log = get_task_logger(__name__)


@celery.task()
def query_subscription_plans(billing_cycle_id: int, subscription_id : Optional[int] = None) -> Union[List, Dict]:
    """
    Get subscriptions plans versions.

        Args:
            billing_cycle_id (int): billing cycle id
            subscription_id (int, optional): optional subscription id
        
        Returns:
            objects: returned from query
        
        Raises:
            NotFoundError: if billing cycle or subscription wasn't found
    """
    billing_cycle = get_or_not_found(BillingCycle, billing_cycle_id)
    if subscription_id is None:
        plans = SubscriptionVersion.get_plans_versions(billing_cycle)
    else:
        subscription = get_or_not_found(Subscription, subscription_id)
        plans = SubscriptionVersion.get_subscription_versions(billing_cycle, subscription)
    return plans
