from datetime import datetime, timedelta

from flask_testing import TestCase

from src import create_app
from src.models.base import db
from src.models.subscriptions import Subscription, SubscriptionVersion, SubscriptionStatus
from src.models.service_codes import Plan
from src.models.cycles import BillingCycle


class TestModels(TestCase):

    SQLALCHEMY_DATABASE_URI = "sqlite:///test_db.sqlite"
    TESTING = True

    def populate_db(self):
        self.plan1 = Plan(
            id = '1',
            description = 'testing plan 1',
            mb_available = 1000,
            is_unlimited = False,
        )
        self.plan2 = Plan(
            id = '2',
            description = 'testing plan 2',
            mb_available = 2000,
            is_unlimited = False,
        )
        db.session.add(self.plan1)
        db.session.add(self.plan2)
        self.billing_cycle = BillingCycle(
            start_date = datetime.now() - timedelta(days=1),
            end_date = datetime.now() + timedelta(days=31),
        )
        db.session.add(self.billing_cycle)
        db.session.commit()
        self.subscription = Subscription(
            phone_number = '1111111111',
            status = SubscriptionStatus.active,
            activation_date = datetime.now(),
            expiry_date = datetime.now() + timedelta(days=30),
            plan_id = self.plan1.id,
        )
        db.session.bulk_save_objects([self.subscription])
        db.session.commit()

    def create_app(self):
        return create_app(self)

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add_subscription_version_correct(self):
        assert True
        subscription = Subscription.query.get(1)
        SubscriptionVersion.add_subscription_version(subscription)
        db.session.commit()
        version = SubscriptionVersion.query.get(1)
        assert version.subscription_id == subscription.id
        assert version.plan_id == subscription.plan_id

    def test_create_version_on_plan_id_changed_correct(self):
        subscription = Subscription.query.get(1)
        subscription.plan_id = self.plan2.id
        db.session.commit()
        version = SubscriptionVersion.query.get(1)
        assert version.subscription_id == subscription.id
        assert version.plan_id == subscription.plan_id

    def test_plan_id_not_changed_version_not_created(self):
        versions_count = SubscriptionVersion.query.count()
        subscription = Subscription.query.get(1)
        subscription.phone_number = '2222222222'
        db.session.commit()
        assert versions_count == SubscriptionVersion.query.count()

    def test_get_subscription_version_correct(self):
        subscription = Subscription.query.get(1)
        subscription.plan_id = self.plan2.id
        db.session.commit()
        subscription.plan_id = self.plan1.id
        db.session.commit()
        versions = [i[0] for i in SubscriptionVersion.get_subscription_versions(
            self.billing_cycle,
            subscription
        )]
        assert [self.plan2.mb_available, self.plan1.mb_available] == versions, versions

    def test_get_plans_versions_correct(self):
        new_subscription = Subscription(
            phone_number = '2321421321',
            status = SubscriptionStatus.active,
            activation_date = datetime.now(),
            expiry_date = datetime.now() + timedelta(days=30),
            plan_id = self.plan1.id,
        )
        db.session.add(new_subscription)
        subscription = Subscription.query.get(1)
        subscription.plan_id = self.plan2.id
        db.session.commit()
        subscription.plan_id = self.plan1.id
        db.session.commit()
        plans = SubscriptionVersion.get_plans_versions(self.billing_cycle)
        assert {
            self.plan1.mb_available: [new_subscription.id, subscription.id],
            self.plan2.mb_available: [subscription.id]
        } == plans, plans
