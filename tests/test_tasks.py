from sys import version
import unittest
from unittest import mock

from src.celery_app import celery
from src.tasks import plans


        # get_or_not_found = mock.MagicMock()
        # get_or_not_found.return_value = True

class TestPlansTasks(unittest.TestCase):

    def setUp(self):
        celery.conf.update(CELERY_ALWAYS_EAGER=True)

    @mock.patch('src.tasks.plans.SubscriptionVersion.get_subscription_versions', return_value=list())
    @mock.patch('src.tasks.plans.get_or_not_found', return_value=True)
    def test_query_subscription_plans_with_subscription_id_ok(self, versions_mock, not_found_mock):
        plans.query_subscription_plans(1, 1)
        assert not_found_mock.called
        assert versions_mock.called

    @mock.patch('src.tasks.plans.SubscriptionVersion.get_plans_versions', return_value=dict())
    @mock.patch('src.tasks.plans.get_or_not_found', return_value=True)
    def test_query_subscription_plans_without_subscription_id_ok(self, versions_mock, not_found_mock):
        plans.query_subscription_plans(1)
        assert not_found_mock.called
        assert versions_mock.called